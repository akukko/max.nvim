local M = {}

M.setup = function() end

M.toggle = function()
  if vim.t.maximized then
    vim.cmd(':wincmd =')
  else
    if vim.fn.winnr('$') == 1 then
      return
    end

    vim.cmd(':wincmd |')
    vim.cmd(':wincmd _')
  end

  vim.t.maximized = not vim.t.maximized
end

return M
