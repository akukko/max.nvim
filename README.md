# max.nvim

A simple window maximize plugin for Neovim.

## Packer installation

```lua
use {
  'https://gitlab.com/akukko/max.nvim'
}
```

## Features

```lua
require('max').toggle()
```


